import 'dotenv/config';

export default
{
  expo: {
    "name": "flight-app",
    "slug": "flight-app",
    "version": "1.0.0",
    "orientation": "portrait",
    "icon": "./assets/icon.png",
    "splash": {
      "image": "./assets/splash.png",
      "resizeMode": "contain",
      "backgroundColor": "#ffffff"
    },
    "updates": {
      "fallbackToCacheTimeout": 0
    },
    "assetBundlePatterns": [
      "**/*"
    ],
    "ios": {
      "supportsTablet": true
    },
    "android": {
      "adaptiveIcon": {
        "foregroundImage": "./assets/adaptive-icon.png",
        "backgroundColor": "#FFFFFF"
      },
      package: "com.myapps.flightapp",
      config: {
        googleSignIn: {
          apiKey : "AIzaSyDyifNjlBuccTBraYPnrvBraLY1xe9VB1s",
          certificateHash: "AF:69:0F:A1:7C:16:41:BE:16:9E:50:1E:37:62:DB:9B:38:3A:BE:6E" // Fingerprint
        }
      }
    },
    "web": {
      "favicon": "./assets/favicon.png"
    },
    "extra": {
      apiKey: process.env.API_KEY,
      authDomain: process.env.AUTH_DOMAIN,
      projectId: process.env.PROJECT_ID,
      storageBucket: process.env.STORAGE_BUCKET,
      messagingSenderId: process.env.MESSAGING_SENDER_ID,
      appId: process.env.APP_ID,
      measurementId: process.env.MEASUREMENTID,
      androidClientId: process.env.ANDROID_CLIENT_ID,
      iosClientId: process.env.IOS_CLIENT_ID,
      standAlone: process.env.STAND_ALONE
    }
  }
}
