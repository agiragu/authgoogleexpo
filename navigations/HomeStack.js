import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { headerOptions } from '../components/Header';
import HomeScreen from '../screens/HomeScreen';

const Stack = createStackNavigator();

export const HomeStack = () => {
  return (
    <Stack.Navigator
      screenOptions={(props) => headerOptions({ ...props }) }
    >
      <Stack.Screen name="home" component={HomeScreen} />
    </Stack.Navigator>
  )
};
