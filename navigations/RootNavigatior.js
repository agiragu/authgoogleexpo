import React, { useContext, useState, useEffect } from 'react';
import { ActivityIndicator, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from '../config/firebase';
import { AuthenticatedUserContext } from '../providers';
import { HomeStack } from './HomeStack';
import { AuthStack } from './AuthStack';

const RootNavigator = () => {
  const { user, setUser } = useContext(AuthenticatedUserContext);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const unsubscribeAuthStateChanged = onAuthStateChanged(
      auth,
      authenticatedUser => {
        authenticatedUser ? setUser(authenticatedUser) : setUser(null);
        setIsLoading(false);
      }
    );

    return unsubscribeAuthStateChanged;
  }, [user]);

  if (isLoading) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
        }}
      >
        <ActivityIndicator size="large" color="black"></ActivityIndicator>
      </View>
    );
  }

  return (
    <NavigationContainer>
      { user ? <HomeStack /> : <AuthStack />}
    </NavigationContainer>
  );
};

export default RootNavigator;
