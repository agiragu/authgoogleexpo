import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { headerOptions } from '../components/Header';

import LoginScreen from '../screens/LoginScreen';

const Stack = createStackNavigator();

export const AuthStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="login"
      screenOptions={(props) => headerOptions({ ...props }) }
    >
      <Stack.Screen name="login" component={LoginScreen} />
    </Stack.Navigator>
  )
};
