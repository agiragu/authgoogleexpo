import React from 'react';
import Constants from 'expo-constants';
import * as Google from 'expo-google-app-auth';
import { GoogleAuthProvider, signInWithCredential } from 'firebase/auth';
import { View, Text, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { auth } from '../config/firebase';
import { fontSize } from '../utils/Responsive';
import FormLogin from '../components/FormLogin';
import SocialGoogle from '../components/SocialGoogle';

const LoginScreen = () => {

  const handleSignIng = () => {
    const config = {
      'iosClientId': Constants.manifest.extra.iosClientId,
      'androidClientId': Constants.manifest.extra.androidClientId,
      'androidStandaloneAppClientId': Constants.manifest.extra.standAlone,
      'scopes': ['profile', 'email']
    }
    Google
      .logInAsync(config)
      .then(async (logInResult) => {
        if (logInResult.type === 'success') {
          const { idToken } = logInResult;
          try {
            const credential = GoogleAuthProvider.credential(idToken)
            await signInWithCredential(auth, credential);
          } catch (err) {
            return Promise.reject(err);
          }
        }
        return Promise.reject();
      })
      .catch(error => {
        console.log(error)
      })
  }

  return (
    <View style={styles.container}>
      <SocialGoogle handleSignIng={handleSignIng} />
      <View style={styles.divider}>
        <View style={styles.line} />
        <Text style={styles.optionText}>Or</Text>
        <View style={styles.line} />
      </View>
      <FormLogin />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
      display: 'flex',
      flex: 1,
      justifyContent: 'space-evenly',
      alignItems: 'center',
  },
  divider: {
      flexDirection: 'row',
      alignItems: 'center',
  },
  line: {
      width: wp('40%'),
      backgroundColor: '#9D9D9D',
      height: 1
  },
  optionText: {
      fontSize: fontSize(18),
      paddingHorizontal: 20
  }
})

export default LoginScreen;
