import { Dimensions } from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

const {height: heightScreen} = Dimensions.get('screen');

export const windowWidth = Dimensions.get('window').width;
export const windowHeight = Dimensions.get('window').height;

export const fontSize = (value) => {
	return RFValue(value, heightScreen);
};
