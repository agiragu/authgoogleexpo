import 'react-native-gesture-handler';
import React from 'react';
import { AuthenticatedUserProvider } from './providers';
import RootNavigator from './navigations/RootNavigatior';

export default function App() {
  return (
    <AuthenticatedUserProvider>
      <RootNavigator />
    </AuthenticatedUserProvider>
  );
}
