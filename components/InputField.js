import React from 'react';
import {View, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {MaterialCommunityIcons} from '@expo/vector-icons';

const InputField = ({
        leftIcon = '',
        iconColor = '#000',
        rightIcon = '',
        inputStyle = {},
        containerStyle = {},
        placeholderTextColor = '#d4d4d4',
        handlePasswordVisibility = () => { },
        ...rest
        }) => {
    return (
        <View style={[styles.container, containerStyle]}>
            {leftIcon ? (
                <MaterialCommunityIcons
                    name={leftIcon}
                    size={20}
                    color={iconColor}
                    style={styles.leftIcon}
                />
            ) : null}
            <TextInput
                {...rest}
                placeholderTextColor={placeholderTextColor}
                style={[styles.input, inputStyle]}
            />
            {rightIcon ? (
                <TouchableOpacity onPress={handlePasswordVisibility}>
                    <MaterialCommunityIcons
                        name={rightIcon}
                        size={20}
                        color={iconColor}
                        style={styles.rightIcon}
                    />
                </TouchableOpacity>
            ) : null}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 12,
        alignItems: 'center',
        width: wp('78%'),
    },
    leftIcon: {
        marginRight: 10
    },
    input: {
        flex: 1,
        width: wp('100%'),
        fontSize: 18,
    },
    rightIcon: {
        alignSelf: 'center',
        marginLeft: 10
    }
});

export default InputField;
