import React from 'react'
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { fontSize } from '../utils/Responsive'

const SocialGoogle = ({ handleSignIng }) => {
  return (
    <View style={styles.container}>
      <Text style={ styles.title }>Login with</Text>

      <TouchableOpacity style={{ alignItems: 'center'}} onPress={handleSignIng}>
        <Image
          resizeMode="stretch"
          accessibilityRole={'image'}
          source={require('../assets/google-icon.png')}
          style={{ width: wp('13'), height: hp('6') }}
        />
        <Text style={ styles.textSign }>Sign in with Google</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: fontSize(18),
    paddingBottom: 20
  },
  textSign: {
    fontSize: fontSize(14),
    textDecorationLine: 'underline'
  }
})

export default SocialGoogle
