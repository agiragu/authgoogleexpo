import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { signOut } from 'firebase/auth';
import { auth } from '../config/firebase';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Ionicons } from '@expo/vector-icons';
import { fontSize } from '../utils/Responsive';

export const headerOptions = ({ route }) => {

  const handleSignOut = async () => {
    try {
      await signOut(auth)
    } catch (error) {
      console.log(error);
    }
  };

  const header = {
    headerTitle: (props) => ['login', 'signup'].includes(route.name) ? <Header {...props} /> : null,
    headerLeft: (props ) => !['login', 'signup'].includes(route.name) ? <Header {...props} styleCustom={{ paddingLeft: 15 }}/> : null,
    headerTitleAlign: ['login', 'signup'].includes(route.name) ? 'center': null,
    headerRight: () => {
      const routeExclude = ['login'];
      if (!routeExclude.includes(route.name)) {
        return (
          <View style={{ paddingRight: 10 }}>
            <TouchableOpacity
              style={{
                borderBottomWidth: 0.1,
                elevation: 11,
                shadowColor: '#000',
                shadowOffset: {
                  width: 10,
                  height: -12,
                },
                shadowOpacity: 0.8,
                shadowRadius: 3,
                padding: 5,
              }}
              onPress={handleSignOut}
            >
              <Ionicons name="exit-outline" size={37} color="white" />
            </TouchableOpacity>
          </View>
        );
      }
    },
    headerStyle: {
      height: hp(15),
      shadowColor: '#000',
      elevation: 12,
      shadowOpacity: 10,
      borderBottomWidth: 5,
      borderBottomRightRadius: 20,
      borderBottomLeftRadius: 20,
      backgroundColor: '#8957DF',
    },
  };
  return header;
};

const Header = (props) => {
  return (
    <View style={[styles.container, props?.styleCustom]}>
      <Text style={styles.text}>Flight app</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  text: {
    fontSize: fontSize(35),
    color: '#ffffff',
    fontWeight: 'bold',
  },
  textWork: {
    fontWeight: 'bold',
  },
});
