import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { signInWithEmailAndPassword } from 'firebase/auth';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { auth } from '../config/firebase';
import { fontSize } from '../utils/Responsive';
import InputField from './InputField';
import Button from './Button';

const FormLogin = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordVisibility, setPasswordVisibility] = useState(true);
  const [rightIcon, setRightIcon] = useState('eye');
  const [loginError, setLoginError] = useState('');

  const onLogin = async () => {
    try {
      if (email !== '' && password !== '') {
        await signInWithEmailAndPassword(auth, email, password);
      }
    } catch (error) {
      console.log(error.message)
      setLoginError(error.message);
    }
  };

  const handlePasswordVisibility = () => {
    if (rightIcon === 'eye') {
      setRightIcon('eye-off');
      setPasswordVisibility(!passwordVisibility);
    } else if (rightIcon === 'eye-off') {
      setRightIcon('eye');
      setPasswordVisibility(!passwordVisibility);
    }
  };

  return (
    <View>
      <InputField
        inputStyle={styles.inputStyle}
        containerStyle={styles.containerInput}
        placeholder="User Name"
        autoCapitalize="none"
        keyboardType="email-address"
        textContentType="emailAddress"
        autoFocus={true}
        value={email}
        onChangeText={(text) => setEmail(text)}
      />
      <InputField
        inputStyle={styles.inputStyle}
        containerStyle={styles.containerInput}
        placeholder="Password"
        autoCapitalize="none"
        autoCorrect={false}
        secureTextEntry={passwordVisibility}
        textContentType="password"
        rightIcon={rightIcon}
        value={password}
        onChangeText={(text) => setPassword(text)}
        handlePasswordVisibility={handlePasswordVisibility}
      />
      <View style={styles.forgotPassword}>
        <Text style={styles.textForgot}>Forgot Password?</Text>
      </View>
      <View style={styles.containerButtons}>
        <Button style={styles.button} action={onLogin} name="Log In" />
        <Button style={styles.button} action={() => {}} name="Create" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor: 'red',
    marginHorizontal: 100,
  },
  title: {
    fontSize: 24,
    fontWeight: '600',
    color: '#111',
    alignSelf: 'center',
    paddingBottom: 24,
  },
  button: {
    backgroundColor: '#8957DF',
    width: wp('40'),
    borderRadius: 18,
    height: hp('4.5'),
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
  containerButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  forgotPassword: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: -15,
    paddingBottom: 20,
  },
  textForgot: {
    fontSize: fontSize(12),
    textDecorationLine: 'underline',
    color: '#d4d4d4',
  },
  containerInput: {
    backgroundColor: '#fff',
    marginBottom: 20,
    borderRadius: 15,
    width: wp('90%'),
    borderWidth: 1,
    borderColor: '#e7e7e7',
  },
  inputStyle: {
    fontSize: fontSize(14),
  },
});

export default FormLogin;
